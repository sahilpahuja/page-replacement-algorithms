# Page Replacement Algorithm Simulator

A Java program with swing GUI which accepts list of page accesses, no. of frames and the page replacement algorithm to be used and produces output:

* simulation of each frame with description (whether page was hit or miss)
* no. of page hits
* no. of page faults
* and total Hit Ratio

Supports FIFO, LRU, and OPTIMAL algorithms.


## Prerequisites
Java version used 1.8.0_211

## Output

### FIFO
![FIFO](output/FIFO.png)

### LRU
![LRU](output/LRU.png)

### Optimal
![Optimal](output/Optimal.png)
