
import javax.swing.JPanel;

/**
 *
 * @author sahil
 */
public class Algo {
    int frameSize, hits, faults;
    float hitRatio;
    int pages[];
    JPanel panel;
    public Algo(JPanel panelToAdd, int pages[], int frameSize){
        this.panel = panelToAdd;
        this.pages = pages;
        this.frameSize = frameSize;
    }
    public void FIFO(){
        int ptr=0;
        Integer stack[];
        stack = new Integer[frameSize];
        boolean hitCurrentPage;
        for(int i=0; i<pages.length; i++){
            hitCurrentPage = false;
            if(IntegerUtil.contains(stack, pages[i]) != -1){
                hits++;
                hitCurrentPage = true;
            }else{
                // Circular
                if(ptr == frameSize){
                    ptr = 0;
                }
                stack[ptr++] = pages[i];
            }
            int pageToBeMarked = -1;
            if (i < frameSize-1) {
                pageToBeMarked = 0;
            } else {
                pageToBeMarked = ptr%frameSize;
            }
            panel.add(new FramesBlock(stack, pages[i]+"", hitCurrentPage, pageToBeMarked));
        }
        calculateHitRatio();
    }
    
    public void LRU(){
        Integer frames[] = new Integer[frameSize];
        NodeList.Node found;
        NodeList<Integer> queueOfIndex = new NodeList<>();
        boolean hitCurrentPage;
        int foundAt = -1;
        
        for(int i=0; i<pages.length; i++){
            hitCurrentPage = false;
            if (i < frameSize) {
                frames[i] = pages[i];
                queueOfIndex.insertAtEnd(i);
            } else {
                if((foundAt = IntegerUtil.contains(frames, pages[i])) != -1){
                    queueOfIndex.delete(queueOfIndex.exists(foundAt));
                    hits++;
                    hitCurrentPage = true;
                }else{
                    frames[queueOfIndex.head.data] = pages[i];
                    foundAt = queueOfIndex.head.data;
                    queueOfIndex.deleteStart();
                }
                queueOfIndex.insertAtEnd(foundAt);
            }
            panel.add(new FramesBlock(frames, pages[i]+"", hitCurrentPage, queueOfIndex.head.data));
        }
        
        calculateHitRatio();
    }
    
    public void optimal(){
        Integer frames[] = new Integer[frameSize];
        NodeList.Node found = null, tempNode;
        NodeList<Pair<Integer, Integer>> queueOfPairOfFrameIndexAndElementIndex = new NodeList<>();
        boolean hitCurrentPage;
        int numOfComingNodesToBeChecked;
        
        
        int foundAt = -1;
        
        for(int i=0; i<pages.length; i++){
            hitCurrentPage = false;
            // filling frame until full
            if (i < frameSize) {
                frames[i] = pages[i];
                queueOfPairOfFrameIndexAndElementIndex.insertAtEnd(new Pair(i, Integer.MAX_VALUE));
            } else {
                /* If page is coming for the first time after frame is full. Calculate future Indexes for pages already present in freme
                 * and sort descending so if the next page comes the index of first element in queueOfPairOfFrameIndexAndElementIndex
                 * will be extracted and that index will be replaces in frames array
                */
                if (i == frameSize) {
                    numOfComingNodesToBeChecked = frameSize;
                    for(int j=i; j<pages.length && numOfComingNodesToBeChecked > 0; ++j){
                        if((foundAt = IntegerUtil.contains(frames, pages[j])) != -1){
                            found = queueOfPairOfFrameIndexAndElementIndex.exists(new Pair(foundAt, Integer.MAX_VALUE));
                            if(
                                (tempNode = queueOfPairOfFrameIndexAndElementIndex.exists((Pair<Integer, Integer>)found.data)) != null
                                && ((Pair<Integer, Integer>)tempNode.data).second > i
                                && ((Pair<Integer, Integer>)tempNode.data).second < pages.length
                            ) {
                                continue;
                            }
                            queueOfPairOfFrameIndexAndElementIndex.delete(found);
                            ((Pair<Integer, Integer>)(found.data)).second = j;
                            queueOfPairOfFrameIndexAndElementIndex.insertDescending((Pair<Integer, Integer>)found.data);
                            --numOfComingNodesToBeChecked;
                        }
                    }
                    frames[queueOfPairOfFrameIndexAndElementIndex.head.data.first] = pages[i];
                    found = queueOfPairOfFrameIndexAndElementIndex.head;
                    queueOfPairOfFrameIndexAndElementIndex.deleteStart();
                }
                
                else {
                    // If the current page is present in frames then page hit will occur and new future index will be calculated
                    if((foundAt = IntegerUtil.contains(frames, pages[i])) != -1){
                        found = queueOfPairOfFrameIndexAndElementIndex.exists(new Pair(foundAt, Integer.MAX_VALUE));
                        queueOfPairOfFrameIndexAndElementIndex.delete(found);
                        hits++;
                        hitCurrentPage = true;
                    }
                    /*first element in queueOfPairOfFrameIndexAndElementIndex will be extracted 
                     * and that index will be replaced in frames array.
                     * And new future index for current page will be calculated
                    */
                    else{
                        frames[queueOfPairOfFrameIndexAndElementIndex.head.data.first] = pages[i];
                        found = queueOfPairOfFrameIndexAndElementIndex.head;
                        queueOfPairOfFrameIndexAndElementIndex.deleteStart();
                    }
                }
                
                boolean isPagePresentInFuture = false;
                for(int j=i+1; j<pages.length; j++){
                    if(frames[((Pair<Integer, Integer>)(found.data)).first] == pages[j]){
                        isPagePresentInFuture = true;
                        ((Pair<Integer, Integer>)(found.data)).second = j;
                        break;
                    }
                }
                if (!isPagePresentInFuture) {
                    ((Pair<Integer, Integer>)(found.data)).second = Integer.MAX_VALUE;
                }
                queueOfPairOfFrameIndexAndElementIndex.insertDescending((Pair<Integer, Integer>)found.data);
                queueOfPairOfFrameIndexAndElementIndex.printList();
            }
            panel.add(new FramesBlock(frames, pages[i]+"", hitCurrentPage, -1));
        }
        calculateHitRatio();
    }

    private void calculateHitRatio() {
        faults = pages.length-hits;
        // hits/ph+pf
        hitRatio = ((float)hits*(float)100)/((float)(hits+faults));
    }
}
