

/**
 *
 * @author sahil
 * @param <A>
 */
public class NodeList<A extends Comparable<A>> {
    Node head, tail;
    private int numOfNodes=0;
    class Node{
        A data;
        Node next;
        public Node(A data) {
            this.data = data;
        }
    }
    public void insertAfter(A after, A data){
        Node current = head;
        while(current != null && !current.data.equals(after)){
            current = current.next;
        }
        if(current != null){
            Node newNode = new Node(data);
            newNode.next = current.next;
            current.next = newNode;
        }
    }
    public void insertDescending(A data){
        Node current = head, follow = null;
        while(current != null && current.data.compareTo(data) >= 0){
            follow = current;
            current = current.next;
        }
        if (current == null) {
            insertAtEnd(data);
            return;
        }
        if (follow == null) {
            insertAtBeg(data);
            return;
        }
        Node newNode = new Node(data);
        newNode.next = follow.next;
        follow.next = newNode;
    }
    public void replaceFirst(A toReplace, A replaceWith){
        Node current = head;
        while(current != null && current.data != toReplace){
            current = current.next;
        }
        if(current != null){
            current.data = replaceWith;
        }
    }
    public void insertAtBeg(A data){
        if(head == null){
            head = tail = new Node(data);
        }else{
            Node node = new Node(data);
            node.next = head;
            head = node;
        }
        numOfNodes++;
    }
    public void insertAtEnd(A data){
        if(tail == null){
            head = tail = new Node(data);
        }else{
            tail.next = new Node(data);
            tail = tail.next;
        }
        numOfNodes++;
    }
    public void deleteStart(){
        head = head.next;
        numOfNodes--;
    }
    public void delete(Node node){
        Node follow = null, current = head;
        if(node == null){
            return;
        }
        while(current != null && current != node){
            follow = current;
            current = current.next;
        }
        if(current!=null){
            if(current == head){
                head = current.next;
            }else{
                if(current == tail){
                    tail = follow;
                }
                follow.next = current.next;
            }
            numOfNodes--;
        }
    }
    public boolean isEmpty(){
        return head==null;
    }
    public Node exists(A key){
        Node current = head;
        while(current != null){
            if(current.data.equals(key)){
                return current;
            }
            current = current.next;
        }
        return null;
    }
    public void printList() 
    { 
        Node curr = head; 
        while (curr != null) 
        { 
            System.out.print(curr.data+" "); 
            curr = curr.next; 
        }
        System.out.println("");
    }
    public int size(){
        return numOfNodes;
    }
}
