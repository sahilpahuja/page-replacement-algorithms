/**
 *
 * @author sahil
 */
public class IntegerUtil {
    public static int contains(Integer arr[], int key){
        for(int i=0; i<arr.length; i++){
            if(arr[i] == null){
                continue;
            }
            if(arr[i] == key){
                return i;
            }
        }
        return -1;
    }
}
